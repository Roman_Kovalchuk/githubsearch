//
//  UITableViewCell+Extensions.swift
//  GitHubSearch
//
//  Created by Roman Kovalchuk on 20/01/2019.
//  Copyright © 2019 Roman Kovalchuk. All rights reserved.
//

import UIKit

extension UITableViewCell: Describable {}
