//
//  String+Extensions.swift
//  GitHubSearch
//
//  Created by Roman Kovalchuk on 23/01/2019.
//  Copyright © 2019 Roman Kovalchuk. All rights reserved.
//

import Foundation

extension String {

    var URLEscapedString: String {
        return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!
    }

}
