//
//  UITableView+Extensions.swift
//  GitHubSearch
//
//  Created by Roman Kovalchuk on 19/01/2019.
//  Copyright © 2019 Roman Kovalchuk. All rights reserved.
//

import UIKit

extension UITableView {

    func dequeueCell<Cell: UITableViewCell>(withIdentifier identifier: String, for indexPath: IndexPath) -> Cell {
        // swiftlint:disable:next force_cast
        return dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! Cell
    }

}
