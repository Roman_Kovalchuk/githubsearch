//
//  RepositoryCellViewModel.swift
//  GitHubSearch
//
//  Created by Roman Kovalchuk on 19/01/2019.
//  Copyright © 2019 Roman Kovalchuk. All rights reserved.
//

import Foundation

final class RepositoryCellViewModel {

    var repository = Observable<Repository>()
    var fullName: String?
    var repoDescription: String?

    init(repository: Repository) {
        self.repository.value = repository
        self.fullName         = repository.fullName
        self.repoDescription  = repository.repoDescription
    }

}
