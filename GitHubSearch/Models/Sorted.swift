//
//  Sorted.swift
//  GitHubSearch
//
//  Created by Roman Kovalchuk on 14/02/2019.
//  Copyright © 2019 Roman Kovalchuk. All rights reserved.
//

import Foundation

struct Sorted {
    var key: String
    var ascending: Bool = true
}
