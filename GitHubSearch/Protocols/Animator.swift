//
//  Animator.swift
//  GitHubSearch
//
//  Created by Roman Kovalchuk on 01/02/2019.
//  Copyright © 2019 Roman Kovalchuk. All rights reserved.
//

import UIKit

protocol Animator: UIViewControllerAnimatedTransitioning {
    var isPresenting: Bool { get set }
}
