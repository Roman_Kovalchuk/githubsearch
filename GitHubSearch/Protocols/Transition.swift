//
//  Transition.swift
//  GitHubSearch
//
//  Created by Roman Kovalchuk on 01/02/2019.
//  Copyright © 2019 Roman Kovalchuk. All rights reserved.
//

import UIKit

protocol Transition: AnyObject {
    /// Should be declared as weak.
    var viewController: UIViewController? { get set }

    func open(_ viewController: UIViewController)
    func close(_ viewController: UIViewController)
}
