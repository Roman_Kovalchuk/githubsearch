//
//  DetailsRoute.swift
//  GitHubSearch
//
//  Created by Roman Kovalchuk on 01/02/2019.
//  Copyright © 2019 Roman Kovalchuk. All rights reserved.
//

import Foundation

protocol DetailsRoute {
    var detailsTransition: Transition { get }
    func openDetails(for repository: Repository)
}

extension DetailsRoute where Self: RouterProtocol {

    var detailsTransition: Transition {
        return PushTransition()
    }

    func openDetails(for repository: Repository) {
        let router = DetailsRouter()
        let detailsViewModel = DetailsViewModel(repository: repository, router: router)
        let detailsViewController = DetailsViewController(viewModel: detailsViewModel)
        router.viewController = detailsViewController

        let transition = detailsTransition
        router.openTransition = transition
        open(detailsViewController, transition: transition)
    }

}
